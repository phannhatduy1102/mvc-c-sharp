using BookEntityNS;
using MenuInterfaceNS;
using BookShelfEntityNS;
namespace DatabaseNS
{
    class Database
    {
        private static volatile Database? instance;
        private static readonly object lockObject = new();

        private readonly List<IMenu> primaryMenu = new()
        {
            new PrimaryMenu { Id = 1, Name = "Tao tu sach" },
            new PrimaryMenu { Id = 2, Name = "Xem sach" },
            new PrimaryMenu { Id = 3, Name = "Xoa sach" },
            new PrimaryMenu { Id = 4, Name = "Them sach" }
        };

        private List<IBookShelf> bookShelves = new();




        public static Database Instance
        {
            get
            {
                if (instance == null)
                {
                    lock (lockObject)
                    {
                        instance ??= new Database();
                    }
                }
                return instance;
            }
        }

        // thêm sách vào tủ theo id tủ
        public IBookShelf? AddBookToBookShelf(IBook book, int IdBookShelf)
        {
            try
            {
                IBookShelf? FindBookShelf = bookShelves.Find(BookShelf => BookShelf.Id == IdBookShelf);

                FindBookShelf?.ListBook.Add(book);
                return FindBookShelf;
            }
            catch (Exception)
            {

                return null;

            }

        }

        // lấy ra danh sách sách trong 1 tủ nào đó theo id
        public List<IBook> GetList(int IdBookShelf)
        {
            IBookShelf? FindBookShelf = bookShelves.Find(BookShelf => BookShelf.Id == IdBookShelf);
            if (FindBookShelf != null)
            {
                return FindBookShelf.ListBook;
            }
            else
            {
                return new List<IBook>();
            }
        }

        public List<IBookShelf> BookShelves
        {
            get { return bookShelves; }
            set { bookShelves = value; }
        }
        public IBookShelf CreateBookShelf(IBookShelf bookShelf)
        {
            bookShelves.Add(bookShelf);
            return bookShelf;
        }


        public bool RemoveBook(int idBook, int idBookShelf)
        {
            try
            {
                IBookShelf? FindBookShelf = bookShelves.Find(BookShelf => BookShelf.Id == idBookShelf);
                int indexBook = FindBookShelf.ListBook.FindIndex(IBook => IBook.Id ==idBook);
                FindBookShelf?.ListBook.RemoveAt(indexBook);
                return true;
            }
            catch (Exception)
            {

                return false;
            }
        }
        public List<IMenu> PrimaryMenu
        {
            get
            {
                return primaryMenu;
            }
        }
    }
}