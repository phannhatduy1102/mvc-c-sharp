using BookShelfEntityNS;

namespace BookShelfHelperNS
{
    class BookShelfHelper
    {
        public static bool IsValidBookShelfId(List<IBookShelf> bookShelves, int IdBookShelf)
        {
            try
            {
                IBookShelf? FindBookShelf = bookShelves.Find(BookShelf => BookShelf.Id == IdBookShelf);
                return true;
            }
            catch (Exception)
            {
                return false;
            }
        }
    }
}