
namespace UtilsNS
{
    class Utils
    {
        public static bool IsListNotEmpty<T> (List<T> items)
        {
            return items.Count > 0;
        }
    }
}