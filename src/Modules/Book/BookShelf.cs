

using BookEntityNS;
using BookShelfControllerNS;
using BookShelfEntityNS;

namespace BookShelfModuleNS
{
    class BookShelfModule
    {
        private static volatile BookShelfModule? instance;
        private static readonly object lockObject = new();

        public BookShelfController BookShelfController = new();

        private BookShelfModule()
        {

        }

        public static BookShelfModule Instance
        {
            get
            {
                if (instance == null)
                {
                    lock (lockObject)
                    {
                        instance ??= new BookShelfModule();
                    }
                }
                return instance;
            }
        }
    }

}