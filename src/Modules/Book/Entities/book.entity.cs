namespace BookEntityNS
{

    public interface IBook
    {
        public int Id { get; set; }
        public string Name {get; set;}
    }

    public class Book : IBook
    {
        public int Id { get; set; }
        public string Name { get; set; }

        public Book(int id, string name)
        {
            Id = id;
            Name = name;
        }
    }
}