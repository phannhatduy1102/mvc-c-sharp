
using BookEntityNS;

namespace BookShelfEntityNS
{
    public interface IBookShelf
    {
        public int Id { get; set; }
        public string Name { get; set; }
        public List<IBook> ListBook { get; set; }
    }
    public class BookShelf : IBookShelf
    {
        public int Id { get; set; }
        public string Name { get; set; }
        public List<IBook> ListBook { get; set; }
        public BookShelf(int id, string name)
        {
            Id = id;
            Name = name;
            ListBook = new();
        }

    }

}