
using BookControllerNS;
using BookShelfControllerNS;

namespace BookModuleNS
{
    class BookModule
    {
        private static volatile BookModule? instance;
        private static readonly object lockObject = new();

        public BookController BookController = new();
        

        private BookModule()
        {

        }

        public static BookModule Instance
        {
            get
            {
                if (instance == null)
                {
                    lock (lockObject)
                    {
                        instance ??= new BookModule();
                    }
                }
                return instance;
            }
        }
    }

}