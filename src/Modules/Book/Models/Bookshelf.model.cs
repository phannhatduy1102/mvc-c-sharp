
using BookEntityNS;
using BookShelfEntityNS;
using DatabaseNS;

namespace BookShelfModelNS
{
    class BookShelfModel
    {
        public IBookShelf Create(IBookShelf bookShelf)
        {
            return Database.Instance.CreateBookShelf(bookShelf);
        }

        public List<IBookShelf> GetList()
        {
            return Database.Instance.BookShelves;
        }
        public static IBookShelf? CreateBook(IBook book, int IdBookShelf)
        {
            return Database.Instance.AddBookToBookShelf(book, IdBookShelf);
        }
        public static List<IBook> GetList(int IdBookShelf)
        {
            return Database.Instance.GetList(IdBookShelf);
        }
    }
}