using BookEntityNS;
using DatabaseNS;
using AppControllerNS;
using BookShelfEntityNS;
namespace BookModelNS
{
    class BookModel
    {

        public IBookShelf? Create(IBook book, int idBookShelf)
        {
            return Database.Instance.AddBookToBookShelf(book, idBookShelf);
        }

        public List<IBook> GetList(int idBookShelf)
        {
            return Database.Instance.GetList(idBookShelf);
        }
        public bool Remove(int idBook, int idBookShelf)
        {
            return Database.Instance.RemoveBook(idBook, idBookShelf);
        }


    }
}