using BookEntityNS;
using BookModelNS;
using BookShelfEntityNS;

namespace BookControllerNS
{
    class BookController
    {
        private readonly BookModel bookModel = new();

        public List<IBook> GetList(int idBookShelf)
        {
            return bookModel.GetList(idBookShelf);
        }

        public bool Remove(int idBook, int idBookShelf)
        {
            return bookModel.Remove(idBook, idBookShelf);
        }

        public IBookShelf? Create(IBook book, int idBookShelf)
        {
            return bookModel.Create(book, idBookShelf);
        }
    }
}