using BookShelfModelNS;
using BookShelfEntityNS;
using BookEntityNS;

namespace BookShelfControllerNS
{
    class BookShelfController
    {
        private BookShelfModel bookShelfModel = new();

        public IBookShelf Create(IBookShelf bookShelf)
        {
            return bookShelfModel.Create(bookShelf);
        }

        public List<IBookShelf> GetList()
        {
            return bookShelfModel.GetList();
        }
        public IBookShelf? CreateBook(IBook book, int IdBookShelf)
        {
            return BookShelfModel.CreateBook(book, IdBookShelf);
        }
        public static List<IBook> GetList(int IdBookShelf)
        {
            return BookShelfModel.GetList(IdBookShelf);
        }
    }
}