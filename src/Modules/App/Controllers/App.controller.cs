using AppMenuModelNS;
using MenuInterfaceNS;
using AppMenuViewNS;
using BookModuleNS;
using BookEntityNS;
using DatabaseNS;
using AppViewBookNS;
using AppModuleNS;
using BookModelNS;
using BookShelfEntityNS;
using AppBookShelfViewNS;
using BookShelfModuleNS;
using BookShelfHelperNS;
using UtilsNS;
namespace AppControllerNS
{
    class AppController
    {
        readonly AppMenuModel appMenuModel = new();
        readonly AppMenuView appMenuView = new();
        readonly AppViewBook appViewBook = new();
        readonly AppBookShelfView appBookShelfView = new();
        readonly List<IMenu> primaryMenu;
        // controller dùng để điều khiển
        public AppController()
        {
            primaryMenu = appMenuModel.GetPrimaryMenu();
            Index();
        }

        public void Index()
        {
            while (true)
            {
                int option = appMenuView.PrimaryMenu(primaryMenu);

                switch (option)
                {
                    case 1:
                        CreatedBookShelf();
                        break;
                    case 2:
                        ShowBooks();
                        break;
                    case 3:
                        RemoveBook();
                        break;
                    case 4:
                        AddBook();
                        break;
                    default:
                        break;
                }
            }
        }

        // tạo tủ sách add nó vào list tủ sách
        public void CreatedBookShelf()
        {
            // gọi view lưu trữ info tủ sách vừa tạo qua controller
            IBookShelf NewBookShelf = appBookShelfView.FillBookShelf();
            IBookShelf CreateBookShelf = BookShelfModule.Instance.BookShelfController.Create(NewBookShelf);
            ShowBookShelves();
        }

        public void ShowBookShelves()
        {
            List<IBookShelf> bookShelves = BookShelfModule.Instance.BookShelfController.GetList();
            appBookShelfView.ShowBookShelf(bookShelves);
        }

        // xem danh sach sach 
        public void ShowBooks()
        {
            // lấy danh sách tủ sách 
            List<IBookShelf> bookShelves = BookShelfModule.Instance.BookShelfController.GetList();
            // kiểm tra xem list này có tủ nào không
            bool isExistBookShelf = Utils.IsListNotEmpty(bookShelves);
            if (isExistBookShelf) // nếu có tủ 
            {
                // nhập id tủ sách muốn xem sách
                int IdBookShelf = appBookShelfView.AskForBookShelfId();

                bool IDBookShelf = BookShelfHelper.IsValidBookShelfId(bookShelves, IdBookShelf);
                if (IDBookShelf) // nếu id tủ tồn tại
                {
                    // Kiểm tra điều kiện isExistBook
                    bool isExistBook = Utils.IsListNotEmpty(BookModule.Instance.BookController.GetList(IdBookShelf));
                    // Hiển thị thông tin sách tùy thuộc vào điều kiện isExistBook
                    if (isExistBook)
                    {
                        appViewBook.ShowBooks(BookModule.Instance.BookController.GetList(IdBookShelf));
                    }
                    else
                    {
                        appViewBook.PrintInfoListBook();
                    }
                }
                else // nếu không có id tủ
                {
                    appBookShelfView.NotifyIdBookShelf();
                }
            }
            // nếu không có tủ
            else
            {
                appBookShelfView.NotifyShowBookShelf();
            }

        }

        public void AddBook()
        {
            //lấy danh sách tủ sách
            List<IBookShelf> bookShelves = BookShelfModule.Instance.BookShelfController.GetList();
            // kiểm tra đã có tủ sách chưa

            bool isExistBookShelf = Utils.IsListNotEmpty(bookShelves);
            // nếu có tủ 
            if (isExistBookShelf)
            {
                // cho user nhập id tủ để thêm sách
                int IdBookShelf = appBookShelfView.AddBookToBookShelfById();
                // nếu id tủ có tồn tại thì trả về đúng sai
                bool IDBookShelf = BookShelfHelper.IsValidBookShelfId(bookShelves, IdBookShelf);
                if (IDBookShelf) // nếu id tủ sách tồn tại
                {
                    // thì tiến hành tạo cuốn sách 
                    IBook NewBook = appViewBook.FillInfoNewBook(); // user điền thông tin sách, và return về sách vừa tạo
                    // sau đó add sách này vào tủ
                    BookShelfModule.Instance.BookShelfController.CreateBook(NewBook, IdBookShelf);

                    appBookShelfView.NotifyAddBookToBookShelf();

                }
                else // id tủ sách không tồn tại thì in ra thông báo
                {
                    appBookShelfView.NotifyIdBookShelf();
                }
            }

            // nếu chưa có tủ thì in ra thông báo
            else
            {
                appBookShelfView.NotifyAddBookShelf();
            }

        }

        public void RemoveBook()
        {
            //lấy danh sách tủ sách
            List<IBookShelf> bookShelves = BookShelfModule.Instance.BookShelfController.GetList();
            // kiểm tra trong list tủ có tủ nào tồn tại không
            bool isExistBookShelf = Utils.IsListNotEmpty(bookShelves);
            // nếu có tủ 
            if (isExistBookShelf)
            {
                // cho user nhập id tủ
                int idBookShelf = appBookShelfView.FillIdBookShelf();

                // nếu id tủ có tồn tại thì trả về đúng sai
                bool IDBookShelf = BookShelfHelper.IsValidBookShelfId(bookShelves, idBookShelf);

                if (IDBookShelf) // nếu id tủ sách tồn tại
                {
                    // vô tủ đó lấy danh sách sách
                    List<IBook> books = BookShelfControllerNS.BookShelfController.GetList(idBookShelf); // lấy dữ liệu từ DB về

                    // kiểm tra tủ có sách hay không
                    bool isExistBook = Utils.IsListNotEmpty(books);
                    if (isExistBook)
                    {
                        appBookShelfView.NotifyInfoListBook();
                        this.appViewBook.ShowBooks(books);
                        // id sách bạn muốn xóa
                        int idBook = this.appViewBook.FillIdBookToRemove(books);
                        bool isSuccess = BookModule.Instance.BookController.Remove(idBook, idBookShelf);
                        _ = isSuccess ? appViewBook.NotifyBookRemovalSuccess() : appViewBook.NotifyBookRemovalFailure();

                    }
                    else
                    {
                        appViewBook.PrintInfoListBook();
                    }

                }
                // nếu chưa có tủ thì in ra thông báo
                else
                {
                    appBookShelfView.NotifyIdBookShelf();
                }

            }
            else
            {
                appBookShelfView.NotifyDeleteBookShelf();
            }
        }
    }
}
