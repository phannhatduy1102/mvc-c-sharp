using AppControllerNS;

namespace AppModuleNS
{
    class AppModule
    {
        public AppModule()
        {
            _ = new AppController();
        }
    }
}