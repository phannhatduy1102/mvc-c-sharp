using MenuInterfaceNS;
using DatabaseNS;

namespace AppMenuModelNS
{
    class AppMenuModel
    {
        public List<IMenu> GetPrimaryMenu()
        {
            return Database.Instance.PrimaryMenu;
        }
    }
}