
using BookShelfEntityNS;
namespace AppBookShelfViewNS
{
    class AppBookShelfView
    {
        public IBookShelf FillBookShelf()
        {
            Console.WriteLine("Nhap thong tin tu sach: ");

            Console.WriteLine("ID tu sach: ");
            int Id = Convert.ToInt32(Console.ReadLine());

            Console.WriteLine("Ten tu sach: ");
            string? Name = Console.ReadLine();
            Name ??= "";

            BookShelf NewBookShelf = new(Id, Name);
            return NewBookShelf;
        }

        public void ShowBookShelf(List<IBookShelf> bookShelves)
        {
            Console.WriteLine("Tao tu sach thanh cong");
            foreach (BookShelf bookShelf in bookShelves)
            {
                Console.WriteLine("Thong tin tu sach: ");
                Console.WriteLine("ID sach: " + bookShelf.Id);
                Console.WriteLine("Ten sach: " + bookShelf.Name);
            }

        }

        public void NotifyShowBookShelf()
        {
            Console.WriteLine("Hien tai chua co tu sach, khong the xem sach");
            Console.WriteLine("Hay tao tu sach");
        }
        public void NotifyDeleteBookShelf()
        {
            Console.WriteLine("Hien tai chua co tu sach, khong the xoa sach");
            Console.WriteLine("Hay tao tu sach");
        }
        public void NotifyAddBookShelf()
        {
            Console.WriteLine("Hien tai chua co tu sach, khong the them sach");
            Console.WriteLine("Hay tao tu sach");
        }

        public int AddBookToBookShelfById()
        {
            Console.WriteLine("Id tu sach ban muon them sach: ");
            int IDBookShelf = Convert.ToInt32(Console.ReadLine());
            return IDBookShelf;
        }

        public void AskAddBookToBookShelf()
        {
            Console.WriteLine("Ban co muon t");
        }
        public int AskForBookShelfId()
        {
            Console.WriteLine("Id tu sach ban muon xem sach: ");
            int IDBookShelf = Convert.ToInt32(Console.ReadLine());
            return IDBookShelf;
        }
        public void NotifyIdBookShelf()
        {
            Console.WriteLine("ID tu sach khong ton tai");
        }
        public void NotifyAddBookToBookShelf()
        {
            Console.WriteLine("Them sach vao tu thanh cong");
        }

        public int FillIdBookShelf()
        {
            Console.WriteLine("Id tu sach chua sach ban muon xoa: ");
            int IDBookShelf = Convert.ToInt32(Console.ReadLine());
            return IDBookShelf;
        }
        public void NotifyInfoListBook()
        {
            Console.WriteLine("Thong tin tat ca cac sach hien co trong tu");
        }
    }
}