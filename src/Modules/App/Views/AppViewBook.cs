
using BookEntityNS;
using DatabaseNS;

namespace AppViewBookNS
{
    class AppViewBook
    {
        public void ShowBooks(List<IBook> books)
        {

            foreach (IBook book in books)
            {
                Console.WriteLine("Thong tin sach: ");
                Console.WriteLine("ID sach: " + book.Id);
                Console.WriteLine("Ten sach: " + book.Name);
            }
        }

        public void PrintInfoListBook()
        {
            Console.WriteLine("Tu sach hien tai khong co sach");
        }

        public string NotifyBookRemovalSuccess()
        {
            string Notify=  "Xoa sach thanh cong";
            Console.WriteLine(Notify);
            return Notify;
        }
        public string NotifyBookRemovalFailure()
        {
            string Notify=  "Xoa sach khong thanh cong";
            Console.WriteLine(Notify);
            return Notify;
        }

        public int FillIdBookToRemove(List<IBook> book)
        {
            int IdBook = -1;
            Console.WriteLine("Id sach ban muon xoa: ");
            int? UserInput = Convert.ToInt32(Console.ReadLine());
            if (UserInput != null && UserInput >= 0)
            {
                IdBook = Convert.ToInt32(UserInput);
            }
            return IdBook;
        }

        public IBook FillInfoNewBook()
        {
            Console.WriteLine("Nhap thong tin sach: ");
            Console.WriteLine("ID sach: ");
            int Id = Convert.ToInt32(Console.ReadLine());
            Console.WriteLine("Ten sach: ");
            string? Name = Console.ReadLine();
            Name ??= "";
            Book NewBook = new(Id, Name);
            return NewBook;
        }
    }
}