using MenuInterfaceNS;

namespace AppMenuViewNS
{
    class AppMenuView
    {
        public int PrimaryMenu(List<IMenu> primaryMenu)
        {
            int OptionSelected = 0;

            primaryMenu.ForEach(item =>
            {
                Console.Write(item.Id + ": ");
                Console.WriteLine(item.Name);
            });

            Console.WriteLine("Enter option: ");
            string? userInput = Console.ReadLine();

            if (userInput != null && userInput.Length > 0)
            {
                OptionSelected = Convert.ToInt32(userInput);
            }

            return OptionSelected;
        }
    }
}