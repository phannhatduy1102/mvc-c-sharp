namespace MenuInterfaceNS
{
    public interface IMenu
    {
        public int Id { get; set; }
        public string Name { get; set; }
    }
    public class PrimaryMenu : IMenu
    {
        public required int Id { get; set; }
        public required string Name { get; set; }
    }
}